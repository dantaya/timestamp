// This timestamp app provides a list of timestamps that correspond with the params
// the first param is a modifier (s, m, h, d, y) that defines what the rest of the args represents
// the rest of the params are the number of each modifer to add to the current time stamp
//
// the output is a list of timestamps starting with the current unix timestamp and incrementing
// by the modified params

package main

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

// Math to determine how many seconds are in each modifier
var modifiers map[string]int64 = map[string]int64{
	"s": 1,
	"m": 60,
	"h": 60 * 60,
	"d": 24 * 60 * 60,
	"y": 365 * 24 * 60 * 60,
}

func main() {
	// Set detauls
	modifier := "s"
	currentTimestamp := time.Now().UTC().Unix()
	fmt.Println("Current Timestamp")
	fmt.Printf(" - %v\n", currentTimestamp)

	// Get the arguments
	args := os.Args[1:]

	// Loop through the arguments to calculate the timestamps
	for _, arg := range args {
		// Get the argument in numerical form
		value, err := strconv.ParseInt(arg, 10, 64)

		// Argument isn't a number so we need to switch the modifier
		if err != nil {
			// Make sure the modifer is real
			if _, ok := modifiers[arg]; !ok {
				fmt.Printf("\nInvalid modifier attempted, sticking with %v.\n", modifier)
				continue
			}

			// Update the modifer
			fmt.Printf("\nSwitching modifier to: %v\n", arg)
			modifier = arg
			continue
		}

		// Print out the timestamp
		fmt.Printf(" - %v\n", currentTimestamp+(value*modifiers[modifier]))
	}
}
