## Timestamp Generator CLI
Tired of calculated future timestamps when I needed them, I created this tool to help.

### Modifiers
Modifiers are used to adjust your input params so you don't have to work with large numbers.

 - s : second(*s*)
 - m : minute(*s*)
 - h : hour(*s*)
 - d : days(*s*)
 - y : years(*s*)

### Usage
Download the correct file or the repo and build to suit.

Once you have the file you just need to run the command followed by any parameters to pass in.

**Command**
```bash
./timestamp s 1 m 2 3 h 4 d 5 y 6
```

**Output**
```bash
Current Timestamp
 - 1618326904

Switching modifier to: s
 - 1618326905

Switching modifier to: m
 - 1618327024
 - 1618327084

Switching modifier to: h
 - 1618341304

Switching modifier to: d
 - 1618758904

Switching modifier to: y
 - 1807542904
```

The app defaults to the seconds `s` modifier, so if no modifiers are passed in they will be interprected as seconds.